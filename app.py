from flask import Flask, redirect, url_for, render_template, flash, request
from flask_wtf import FlaskForm
from flask_login import (
    LoginManager,
    current_user,
    login_user,
    logout_user,
    login_required,
)
from wtforms import StringField, PasswordField, IntegerField
from wtforms.validators import DataRequired
from modulos.usuario.service import (
    valida_usuario,
    buscar_por_id,
    buscar_todos_alunos,
    salvar_aluno,
)
from modulos.usuario.usuario import Usuario


app = Flask(__name__)
app.config["SECRET_KEY"] = "something only you know"
login = LoginManager(app)
login.login_view = "login"


class LoginForm(FlaskForm):
    login = StringField("Usuário", validators=[DataRequired()])
    senha = PasswordField("Senha", validators=[DataRequired()])


class UsuarioForm(FlaskForm):
    matricula = StringField("Matrícula", validators=[DataRequired()])
    nome = StringField("Nome", validators=[DataRequired()])
    id = IntegerField("Id")


@login.user_loader
def load_user(id):
    return buscar_por_id(int(id))

@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("login"))


@app.route("/login", methods=["POST", "GET"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("root"))
    form = LoginForm()
    if request.method == "GET" or not form.validate_on_submit():
        return render_template("login.html", form=form)

    if logged_user := valida_usuario(form.login.data, form.senha.data):
        login_user(logged_user)
        return redirect(url_for("root"))

    flash("Login ou senha inválida")
    return render_template("login.html", form=form)


@app.route("/api/alunos", methods=["GET", "POST"])
def alunos():
    usuarioForm = UsuarioForm()
    if request.method == "GET":
        dados = buscar_todos_alunos()
        return render_template("alunos-ajax.html", alunos=dados)

    if not usuarioForm.validate_on_submit():
        return "Dados do usuário inválidos", 400

    usuario = Usuario(
        usuarioForm.id.data,
        usuarioForm.matricula.data,
        "",
        usuarioForm.nome.data,
        usuarioForm.matricula.data,
    )
    salvar_aluno(usuario)
    flash("Aluno Criado com Sucesso!")
    return f"/{usuario.id}", 201


@app.route("/api/alunos/add")
def add_aluno():
    usuarioForm = UsuarioForm()
    return render_template("cad-aluno-ajax.html", form=usuarioForm)

@app.route("/api/alunos/<int:id>")
def edit_aluno(id):
    aluno = buscar_por_id(id)
    usuarioForm = UsuarioForm()
    usuarioForm.id.data = aluno.id
    usuarioForm.matricula.data = aluno.matricula
    usuarioForm.nome.data = aluno.name
    return render_template("cad-aluno-ajax.html", form=usuarioForm)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
@login_required
def root(path):
    return render_template("index.html")

if __name__ == "__main__":
    app.run()
