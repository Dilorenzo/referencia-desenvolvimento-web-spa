from werkzeug.security import check_password_hash
from .dao import buscar_usuario_pelo_login, buscar_usuario_por_id, buscar_todos, salvar
from .usuario import Usuario


def valida_usuario(login: str, senha: str) -> Usuario:
    usuario = buscar_usuario_pelo_login(login)

    return usuario if usuario and check_password_hash(usuario.password, senha) else None


def buscar_por_id(id: int) -> Usuario:
    return buscar_usuario_por_id(id)

def buscar_todos_alunos() -> list:
    return buscar_todos()

def salvar_aluno(usuario: Usuario) -> bool:
    usuario.papel = 'ALUNO'
    return salvar(usuario)
