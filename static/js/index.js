const pathToRegex = path => new RegExp("^" + path.replace(/\//g, "\\/").replace(/:\w+/g, "(.+)") + "$");

const navigateTo = url => {
    history.pushState(null, null, url);
    router();
}

const router = async () => {
    const routes = [
        { path: '/', view: '/' },
        { path: '/alunos', view: '/alunos' },
        { path: '/alunos/add', view: '/alunos/add' },
        { path: '/alunos/:id', view: '/alunos/add-ajax' },
    ];

    const potentialMatches = routes.map(route => {
        return {
            ...route,
            view: `/api${location.pathname}`,
            result: location.pathname.match(pathToRegex(route.path)),
        }
    });

    const match = potentialMatches.find(value => value.result !== null);
    
    const request = new XMLHttpRequest();
    request.open('GET', match.view);
    request.onload = () => {
        const response = request.responseText;
        const loadedContent = document.getElementById('app');
        loadedContent.innerHTML = response;
        const scripts = loadedContent.getElementsByTagName('script');
        for (var i = 0; i < scripts.length; ++i) {
            var script = scripts[i];        
            eval(script.innerHTML);
          }
    };
    request.send();
};

document.addEventListener("DOMContentLoaded", () => {
    document.body.addEventListener("click", e => {        
        if (e.target.matches("[data-link]") || e.target.parentElement.matches("[data-link]")) {
            const link = e.target.href ?? e.target.parentElement.href;            
            e.preventDefault();
            navigateTo(link);
        }
    });
    router();
});

